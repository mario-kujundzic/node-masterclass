// JSON generated at https://json-generator.com/#
// Load json from file via require
// automatically calls parse
const randomJson = require("./random.json");
console.log(randomJson);

// manually parse via fs
const fs = require("fs");
const randomJson2RAW = fs.readFileSync("./random.json", { encoding: "utf-8" });
console.log(randomJson2RAW);
const randomJson2Parsed = JSON.parse(randomJson2RAW);

// Check if both objects are the same using lodash
const loadsh = require("lodash");
const { isEqual } = loadsh;
console.log("Objects are equal: ", isEqual(randomJson, randomJson2Parsed));

// Transform name, age, gender, favoriteFruit properties

// copy js object to new js object
// CAREFUL about arrays and other objects, will be copied by reference
const newJson = { ...randomJson };
newJson.name = "Mario Kujundzic";
newJson.age = 25;
newJson.gender = "Male";
newJson.favoriteFruit = "strawberry";
console.log(randomJson);
console.log(newJson);
console.log("Objects are equal: ", isEqual(randomJson, newJson));
// Save to new file

fs.writeFileSync("newJson.json", JSON.stringify(newJson));
