import express from "express";
import bodyParser from "body-parser";

// Use Mongoose config
import "./src/db/mongoose.js";

// Use Custom Mongo Driver (monduck)
import "./src/db/monduck.js";


// Use our custom routers
import routers from "./src/routers/index.js";

// Constants
import { SERVER_PORT } from "./src/util/constants.js";
const port = process.env.PORT || SERVER_PORT;

// Define app

const app = express();

const jsonParser = bodyParser.json();
app.use(jsonParser);

routers.forEach((router) => app.use(router));
app.use(express.json());

// START SERVER

app.listen(port, () => {
  console.log("Server started on port", port);
});
