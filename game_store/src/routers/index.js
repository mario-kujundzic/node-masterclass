import userRouter from "./users.js";
import authRouter from "./auth.js";
import purchaseRouter from "./purchase.js";
import categoryRouter from "./category.js";
import productRouter from "./product.js";

const routers = [userRouter, authRouter, purchaseRouter, categoryRouter, productRouter];
export default routers;
