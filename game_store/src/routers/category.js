import express from "express";

import Category from "../models/category.js";
import { authWithRole, ROLES } from "../middleware/auth.js";

const router = new express.Router();

router.get("/category", authWithRole(ROLES.ANY), async (req, res) => {
  try {
    const categories = await Category.find();
    res.send({ categories });
  } catch (e) {
    res.status(400).send("Cannot get categories!");
  }
});

router.post("/category", authWithRole(ROLES.ADMIN), async (req, res) => {
  try {
    const { name, description } = req.body;

    const category = new Category({ name, description });
    await category.save();
    res.send({ category });
  } catch (e) {
    res.status(400).send("Category creation failed!");
  }
});

router.get("/category/:id", authWithRole(ROLES.ANY), async (req, res) => {
  try {
    const category = await Category.findOne({ _id: Number(req.params.id) });
    res.send({ category });
  } catch (e) {
    res.status(400).send("Cannot get category!");
  }
});

router.patch("/category/:id", authWithRole(ROLES.ADMIN), async (req, res) => {
  try {
    const category = await Category.findOne({ _id: Number(req.params.id) });
    // const { name, description, price, quantity, categories[]}
    const allowedUpdates = ["name", "description"];
    const updates = Object.keys(req.body);
    if (updates.length === 0) {
      return res.status(400).send({ error: "Invalid updates!" });
    }
    const isValidOperation = updates.every((update) =>
      allowedUpdates.includes(update)
    );
    if (!isValidOperation) {
      return res.status(400).send({ error: "Invalid updates!" });
    }

    updates.forEach((update) => (category[update] = req.body[update]));
    await category.save();
    res.send({category});
  } catch (e) {
    res.status(400).send("Cannot update category!");
  }
});

router.delete("/category/:id", authWithRole(ROLES.ADMIN), async (req, res) => {
  try {
    // check if any products have this category? return error if so
    const category = await Category.deleteOne({ _id: Number(req.params.id) });
    res.send({ category });
  } catch (e) {
    res.status(400).send("Cannot delete category!");
  }
});

export default router;
