import express from "express";

import User from "../models/user.js";
import { authWithRole, ROLES } from "../middleware/auth.js";

const router = new express.Router();

router.get("/users/me", authWithRole(ROLES.ANY), (req, res) => {
  res.send(req.user);
});

router.get("/users", authWithRole(ROLES.ADMIN), async (req, res) => {
  // pageNum from 0
  const { perPage = 5, pageNum = 0 } = req.body;
  const users = await User.find({})
    .skip(pageNum * perPage)
    .limit(perPage * 1);

  const count = await User.countDocuments();
  res.send({ totalCount: count, users });
});

router.patch("/users/me", authWithRole(ROLES.ANY), async (req, res) => {
  try {
    const currentUser = req.user;
    // const { email, status, password}
    const allowedUpdates = ["email", "userName", "password"];
    const updates = Object.keys(req.body);
    if (updates.length === 0) {
      return res.status(400).send({ error: "Invalid updates!" });
    }
    const isValidOperation = updates.every((update) =>
      allowedUpdates.includes(update)
    );
    if (!isValidOperation) {
      return res.status(400).send({ error: "Invalid updates!" });
    }

    updates.forEach((update) => (currentUser[update] = req.body[update]));
    await currentUser.save();
    res.send(currentUser);
  } catch (e) {
    res.status(500).send();
  }
});

router.delete("/users/me", authWithRole(ROLES.ANY), async (req, res) => {
  try {
    const user = await User.findOneAndDelete({ _id: req.user._id });
    res.send(user);
  } catch (e) {
    res.status(500).send();
  }
});

router.delete("/users/:id", authWithRole(ROLES.ADMIN), async (req, res) => {
  try {
    const user = await User.findByIdAndDelete(req.params.id);

    if (!user) {
      return res.status(404).send();
    }
    res.send(user);
  } catch (e) {
    res.status(500).send();
  }
});

export default router;
