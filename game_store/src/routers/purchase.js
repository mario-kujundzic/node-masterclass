import express from "express";
import { format, parseISO, startOfTomorrow } from "date-fns";

import Purchase from "../models/purchase.js";
import { authWithRole, ROLES } from "../middleware/auth.js";
import Product from "../models/product.js";

const router = new express.Router();

router.get("/purchase", authWithRole(ROLES.USER), async (req, res) => {
  try {
    const {
      perPage = 5,
      pageNum = 0,
      dateFrom = format(new Date(0), "yyyy-MM-dd"),
      dateTo = format(startOfTomorrow(), "yyyy-MM-dd"),
    } = req.body;

    const purchases = await Purchase.find({
      user: req.user._id,
      date: { $lte: parseISO(dateTo), $gte: parseISO(dateFrom) },
    })
      .skip(pageNum * perPage)
      .limit(perPage * 1);

    const count = await Purchase.countDocuments({ user: req.user._id });
    res.send({ totalCount: count, purchases });
  } catch (e) {
    res.status(400).send("Cannot find purchases!");
  }
})
;

router.post("/purchase", authWithRole(ROLES.USER), async (req, res) => {
  try {
    const { products } = req.body;
    let totalAmount = 0;
    await Promise.all(
      products.map(async (productEntry) => {
        const { product, quantity } = productEntry;
        // check if product actually exists
        const foundProduct = await Product.findOne({ _id: product });
        if (foundProduct === null) {
          throw new Exception("Product doesn't exist");
        }
        // check if product quantity is high enough
        if (foundProduct.quantity < quantity) {
          throw new Exception(`Not enough ${foundProduct.name} in stock!`);
        }
        // add to amount
        totalAmount += foundProduct.price * quantity;
      })
    );
    const amount = totalAmount;
    const purchase = new Purchase({
      user: req.user,
      products,
      amount,
      date: new Date(1688216002),
    });
    await purchase.save();
    // if successful, update product quantities on products to keep it up to date
    await Promise.all(
      products.map(async (productEntry) => {
        const { product, quantity } = productEntry;
        let foundProduct = await Product.findOne({ _id: product });
        foundProduct.quantity = foundProduct.quantity - quantity;
        await foundProduct.save();
      })
    );
    res.send({ purchase });
  } catch (e) {
    res.status(400).send("Purchase failed!");
  }
});

router.get("/purchase/:id", authWithRole(ROLES.USER), async (req, res) => {
  try {
    const purchase = await Purchase.findOne({
      _id: Number(req.params.id),
      user: req.user._id,
    });
    res.send({ purchase });
  } catch (e) {
    res.status(400).send("Cannot find purchase!");
  }
});

export default router;
