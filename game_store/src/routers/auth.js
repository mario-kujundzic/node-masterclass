import express from "express";
import User from "../models/user.js";
import { ROLES, authWithRole } from "../middleware/auth.js";

const router = new express.Router();

router.post("/token", async (req, res) => {
  try {
    const user = await User.findByCredentials(
      req.body.userName,
      req.body.password
    );
    const token = await user.generateAuthToken();
    res.send({ user, token });
  } catch (e) {
    res.status(400).send("Unable to login!");
  }
});

router.post("/register", async (req, res) => {
  try {
    const { email, password, userName } = req.body;
    const user = new User({ email, password, userName });
    await user.save();
    const token = await user.generateAuthToken();
    res.send({ user, token });
  } catch (e) {
    res.status(400).send("Register failed!");
  }
});

router.post("/register-admin", async (req, res) => {
  try {
    const { email, password, userName } = req.body;
    const user = new User({ email, password, userName, role: "ADMIN" });
    await user.save();
    res.send({ user });
  } catch (e) {
    res.status(400).send("Register failed!");
  }
});

export default router;
