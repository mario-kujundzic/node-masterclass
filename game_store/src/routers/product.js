import express from "express";

import Product from "../models/product.js";
import { authWithRole, ROLES } from "../middleware/auth.js";

const router = new express.Router();

router.get("/product", authWithRole(ROLES.ANY), async (req, res) => {
  try {
    // pageNum from 0
    const { perPage = 5, pageNum = 0 } = req.body;
    const { count, products } = await Product.paginate({}, {}, pageNum, perPage);
    res.send({ totalCount: count, products });
  } catch (e) {
    res.status(400).send("Cannot get products!");
  }
});

router.post("/product", authWithRole(ROLES.ADMIN), async (req, res) => {
  try {
    const { name, description, price, quantity, categories } = req.body;

    let product = new Product({
      name,
      description,
      price,
      quantity,
      categories,
    });
    product = await product.save();
    res.send({ product });
  } catch (e) {
    res.status(400).send("Product creation failed!");
  }
});

router.get("/product/:id", authWithRole(ROLES.ANY), async (req, res) => {
  try {
    const product = await Product.findOne({ _id: Number(req.params.id) });

    res.send({ product });
  } catch (e) {
    res.status(400).send("Cannot get product!");
  }
});

router.patch("/product/:id", authWithRole(ROLES.ADMIN), async (req, res) => {
  try {
    const product = await Product.findOne({ _id: Number(req.params.id) });
    // const { name, description, price, quantity, categories[]}
    const allowedUpdates = [
      "name",
      "description",
      "price",
      "quantity",
      "categories",
    ];
    const updates = Object.keys(req.body);
    if (updates.length === 0) {
      return res.status(400).send({ error: "Invalid updates!" });
    }
    const isValidOperation = updates.every((update) =>
      allowedUpdates.includes(update)
    );
    if (!isValidOperation) {
      return res.status(400).send({ error: "Invalid updates!" });
    }

    updates.forEach((update) => (product[update] = req.body[update]));
    await product.save();
    res.send({ product });
  } catch (e) {
    res.status(400).send("Cannot update product!");
  }
});

router.delete("/product/:id", authWithRole(ROLES.ADMIN), async (req, res) => {
  try {
    const product = await Product.deleteOne({ _id: Number(req.params.id) });
    res.send({ product });
  } catch (e) {
    res.status(400).send("Cannot delete product!");
  }
});

export default router;
