const JWT_SECRET = "hydrogenbestband";

const DB_URI = "mongodb://127.0.0.1:27017";

const DB_NAME = "game-store-api";

const SERVER_PORT = 3000;

export { JWT_SECRET, DB_NAME, DB_URI, SERVER_PORT };
