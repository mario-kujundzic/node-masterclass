// use native nodeJS mongo driver

import { MonDuckDb } from "../db/monduck.js";
import Category from "./category.js";
import IdCounter from "./idCounter.js";

const PRODUCT_COLLECTION_STRING = "products";

class Product {
  constructor({ name, description, price, quantity, categories, _id }) {
    this.name = name;
    this.description = description;
    this.price = price;
    this.quantity = quantity;
    this.categories = categories;
    this._id = _id;
  }

  async save() {
    const product = await Product.populateCategories(this);
    const products = MonDuckDb.collection(PRODUCT_COLLECTION_STRING);

    // Check uniqueness of name if saving first time
    if (!product._id) {
      const sameProduct = await products.find({ name: this.name }).toArray();
      if (sameProduct.length !== 0) {
        throw new Exception("Product name not unique!");
      }
    }

    const mappedCategories = await Category.find({
      _id: { $in: this.categories },
    });

    if (mappedCategories.length !== product.categories.length) {
      throw new Exception("Not all categories are in database!");
    }

    // Generate ID for saving first time
    if (!product._id) {
      product._id = await IdCounter.getNextId();
      await products.insertOne(product);
    } else {
      // not terribly efficient but oh well
      const originalDocument = await products.findOne({ _id: product._id });
      await products.replaceOne(originalDocument, product);
    }
    return product;
  }
}

// statics

Product.find = async (searchQuery, searchOptions) => {
  const products = await MonDuckDb.collection(PRODUCT_COLLECTION_STRING)
    .find(searchQuery, searchOptions)
    .toArray();
  const mappedProducts = await Promise.all(
    products.map(async (product) => {
      return await Product.populateCategories(product);
    })
  );
  return mappedProducts.map((product) => new Product(product));
};

Product.paginate = async (searchQuery, searchOptions, pageNumber, pageSize) => {
  const products = await MonDuckDb.collection(PRODUCT_COLLECTION_STRING)
    .find(searchQuery, searchOptions)
    .skip(pageNumber * pageSize)
    .limit(pageSize * 1)
    .toArray();
  const mappedProducts = await Promise.all(
    products.map(async (product) => {
      return await Product.populateCategories(product);
    })
  );
  const count = await MonDuckDb.collection(PRODUCT_COLLECTION_STRING).countDocuments();
  return {
    count: count,
    products: mappedProducts.map((product) => new Product(product)),
  };
};

Product.findOne = async (searchQuery, searchOptions) => {
  const products = MonDuckDb.collection(PRODUCT_COLLECTION_STRING);
  const productDocument = await products.findOne(searchQuery, searchOptions);
  if (productDocument === null) {
    return null;
  }
  return new Product(productDocument);
};

Product.populateCategories = async (product) => {
  const mappedCategories = await Category.find({
    _id: { $in: product.categories },
  });
  return { ...product, categories: mappedCategories };
};

Product.deleteOne = async (searchQuery, searchOptions) => {
  const products = MonDuckDb.collection(PRODUCT_COLLECTION_STRING);
  const productDocument = await products.findOneAndDelete(
    searchQuery,
    searchOptions
  );
  if (productDocument === null) {
    return null;
  }
  return new Product(productDocument);
};

export default Product;
