// use mongoose for db mapping

import { Schema, model } from "mongoose";

import IdCounter from "./idCounter.js";

const purchaseSchema = new Schema({
  _id: {
    type: Number,
  },
  products: [
    {
      product: {
        type: Number,
        required: true
      },
      quantity: {
        type: Number,
        required: true,
        validate(value) {
          if (value <= 0) {
            throw new Error("Product quantity must be positive");
          }
        },
      },
    },
  ],
  amount: {
    type: Number,
  },
  user: {
    type: Number,
    required: true,
    ref: "User",
  },
  date: {
    type: Date,
    required: true
  }
});

purchaseSchema.pre("save", async function (next) {
  const purchase = this;

  // Check if _id is undefined (first time saving), generate it if so
  if (purchase._id === undefined) {
    purchase._id = await IdCounter.getNextId();
  }

  next();
});

const Purchase = model("Purchase", purchaseSchema);

export default Purchase;
