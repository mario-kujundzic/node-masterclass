// use mongoose for db mapping

import { Schema, model } from "mongoose";
import validator from "validator";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

import { JWT_SECRET } from "../util/constants.js";
import IdCounter from "./idCounter.js";

const userSchema = new Schema({
  _id: {
    type: Number,
  },
  email: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    unique: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error("Email is invalid!");
      }
    },
  },
  password: {
    type: String,
    required: true,
    minlength: 7,
    trim: true,
    validate(value) {
      if (value.toLowerCase().includes("password")) {
        throw new Error(`Password cannot contain 'password'!`);
      }
    },
  },
  status: {
    type: String,
    default: "REGISTERED",
  },
  userName: {
    type: String,
    required: true,
    unique: true,
  },
  role: {
    type: String,
    default: "USER",
  },
});

// Custom schema methods (instance + static)

userSchema.methods.toJSON = function () {
  const user = this;
  const userObject = user.toObject();

  delete userObject.password;

  return userObject;
};

userSchema.methods.generateAuthToken = async function () {
  const user = this;
  const token = jwt.sign({ _id: user._id.toString() }, JWT_SECRET);
  return token;
};

userSchema.statics.findByCredentials = async (userName, password) => {
  const user = await User.findOne({ userName });
  if (!user) {
    throw new Error("Unable to login!");
  }

  const isMatch = await bcrypt.compare(password, user.password);

  if (!isMatch) {
    throw new Error("Unable to login!");
  }

  return user;
};

userSchema.pre("save", async function (next) {
  const user = this;

  // Hash the plain text password before saving
  if (user.isModified("password")) {
    
    user.password = await bcrypt.hash(user.password, 8);
  }

  // Check if _id is undefined (first time saving), generate it if so
  if (user._id === undefined) {
    user._id = await IdCounter.getNextId();
  }

  next();
});

const User = model("User", userSchema);

export default User;
