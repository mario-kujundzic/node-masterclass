// use mongoose for db mapping

import { Schema, model } from "mongoose";

const idCounterSchema = new Schema({
  count: {
    type: Number,
  },
});

idCounterSchema.statics.getNextId = async () => {
  const counterCollection = await IdCounter.find({});
  if (counterCollection.length === 0) {
    // If counter doesn't exist, create and set to 2, return 1 as first id
    await new IdCounter({ count: 2 }).save();
    return 1;
  } else {
    // If counter exists, return current value and increment
    const counter = counterCollection[0];
    const nextId = counter.count;
    counter.count += 1;
    await counter.save();
    return nextId;
  }
};

const IdCounter = model("IdCounter", idCounterSchema);

export default IdCounter;
