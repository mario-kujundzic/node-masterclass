// use native nodeJS mongo driver

import { MonDuckDb } from "../db/monduck.js";
import IdCounter from "./idCounter.js";

const CATEGORY_COLLECTION_STRING = "categories";

class Category {
  constructor({ name, description, _id }) {
    this.name = name;
    this.description = description;
    this._id = _id;
  }

  async save() {
    const category = this;
    const categories = MonDuckDb.collection(CATEGORY_COLLECTION_STRING);

    // Check uniqueness of name if saving first time
    if (!category._id) {
      const sameCategories = await categories
        .find({ name: this.name })
        .toArray();
      if (sameCategories.length !== 0) {
        throw new Exception("Category name not unique!");
      }
    }
    if (!category._id) {
      category._id = await IdCounter.getNextId();
      await categories.insertOne(category);
    } else {
      const originalDocument = await categories.findOne({ _id: category._id });
      await categories.replaceOne(originalDocument, category);
    }
    return category;
  }

  toJSON() {
    return {
      name: this.name,
      description: this.description,
      _id: this._id,
    };
  }
}

// statics

Category.find = async (searchQuery, searchOptions) => {
  const categories = MonDuckDb.collection(CATEGORY_COLLECTION_STRING);
  const categoriesDocuments = await categories
    .find(searchQuery, searchOptions)
    .toArray();
  return categoriesDocuments.map((cat) => new Category(cat));
};

Category.findOne = async (searchQuery, searchOptions) => {
  const categories = MonDuckDb.collection(CATEGORY_COLLECTION_STRING);
  const categoryDocument = await categories.findOne(searchQuery, searchOptions);
  if (categoryDocument === null) {
    return null;
  }
  return new Category(categoryDocument);
};

Category.deleteOne = async (searchQuery, searchOptions) => {
  const categories = MonDuckDb.collection(CATEGORY_COLLECTION_STRING);
  const productDocument = await categories.findOneAndDelete(
    searchQuery,
    searchOptions
  );
  if (productDocument === null) {
    return null;
  }
  return new Category(productDocument);
};

export default Category;
