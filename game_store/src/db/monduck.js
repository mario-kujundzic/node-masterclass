import { DB_NAME, DB_URI } from "../util/constants.js";
import { MongoClient } from "mongodb";

const MonDuckClient = new MongoClient(DB_URI);
const MonDuckDb = MonDuckClient.db(DB_NAME);

export { MonDuckClient, MonDuckDb };
