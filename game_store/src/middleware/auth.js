import jwt from "jsonwebtoken";
import User from "../models/user.js";
import { JWT_SECRET } from "../util/constants.js";

// Returns middleware function which checks if the user fulfills required roles
const authWithRole = (roles) => {
  return async (req, res, next) => {
    try {
      const token = req.header("Authorization").replace("Bearer ", "");
      const decoded = jwt.verify(token, JWT_SECRET);
      const user = await User.findOne({ _id: decoded["_id"] });

      if (!user) {
        throw new Error();
      }

      // Check user roles

      const userRole = user.role;
      if (!roles.includes(userRole)) {
        throw new Error("403");
      }

      req.user = user;

      next();
    } catch (e) {
      if (e.message === "403") {
        res.status(403).send({ error: "Authorization failed!" });
        return;
      }
      res.status(401).send({ error: "Authentication failed!" });
    }
  };
};

const ROLES = {
  ANY: ["USER", "ADMIN"],
  USER: ["USER"],
  ADMIN: ["ADMIN"],
};

export { authWithRole, ROLES };
