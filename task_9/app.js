import request from "postman-request";

const weatherStackApiKey = "26439118b00f9b3f1ea0c58b301797b4";

const mapBoxApiKey =
  "pk.eyJ1Ijoic2hlanYiLCJhIjoiY2toZGtnYWNqMDVvZzJ6azkzbmJ5emYyOSJ9.THcnrL031ZGldTJW7agTQQ";

const weatherStackBaseUrl = "http://api.weatherstack.com/current";

const getWeatherStackCombinedUrl = (lat, lon) =>
  `${weatherStackBaseUrl}?access_key=${weatherStackApiKey}&units=m&query=${lat},${lon}`;

const mapBoxBaseUrl = "http://api.mapbox.com/geocoding/v5/mapbox.places/";

const getMapBoxCombinedUrl = (location) =>
  `${mapBoxBaseUrl}${encodeURIComponent(
    location
  )}.json?access_token=${mapBoxApiKey}&limit=1`;

const forecast = (data) => {
  const { lat, lon, location } = data;
  const url = getWeatherStackCombinedUrl(lat, lon);
  return new Promise((resolve, reject) => {
    request({ url: url, json: true }, (error, response) => {
      if (error) {
        console.log("Unable to execute request");
        return reject(new Error("Error in request"));
      }
      const data = response.body.current;
      const temp = data.temperature;
      const precip = data.precip;
      return resolve(
        `${location} - It is currently ${temp} degrees C. There is a ${precip} chance of rain.`
      );
    });
  });
};

const geocode = (location) => {
  const url = getMapBoxCombinedUrl(location);
  return new Promise((resolve, reject) => {
    request({ url: url, json: true }, (error, response) => {
      if (error) {
        return reject(new Error("Error with call"));
      }
      if (response.body.features.length === 0) {
        return reject(new Error("Cannot find"));
      }
      const data = {
        lat: response.body.features[0].center[0],
        lon: response.body.features[0].center[1],
        location: response.body.features[0].place_name,
      };
      return resolve(data);
    });
  });
};

geocode("Novi Sad")
  .then((data) => forecast(data)) // important to return here, as we gotta return promise to continue chain
  .then((data) => console.log(data))
  .catch((error) => console.error(error));

// async await version

const testFunction = async () => {
  try {
    const geocodedData = await geocode("Novi Sad");
    const forecastData = await forecast(geocodedData);
    console.log(forecastData);
  } catch (error) {
    console.error(error);
  }
};

testFunction()
