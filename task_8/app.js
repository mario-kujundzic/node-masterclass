// const request = require("request");

// // Request

// const url = "http://jsonplaceholder.typicode.com/posts";

// request({ url: url, json: true }, (error, { body }) => {
//   if (error) {
//     console.error("Error occurred with request " + error);
//     return;
//   }
//     console.log(body);
// });

// // HTTP request

// const http = require("http");

// const httpRequest = http.request(url, (response) => {
//   let data = "";

//   response.on("data", (chunk) => {
//     data = data + chunk.toString();
//   });
//   response.on("end", () => {
//     console.log(data);
//     return data;
//   });
// });

// httpRequest.end();

const request = require("request");
const fs = require("fs");
const yargs = require("yargs");
const { hideBin } = require("yargs/helpers");

yargs.parse();
const userId = hideBin(process.argv)[0];

// Request

const url = "http://jsonplaceholder.typicode.com/posts";

const getFilteredData = (filterUserId, callback) =>
  request({ url: url, json: true }, (error, { body }) => {
    if (error) {
      console.error("Error occurred with request " + error);
      return;
    }
    if (body.every((post) => post.userId != filterUserId)) {
      // if we don't have any filter items, return error
      callback(`No items with id ${filterUserId}`, undefined);
      return;
    }
    const filteredBody = body.filter((post) => post.userId != filterUserId);
    callback(undefined, filteredBody);
  });

getFilteredData(userId, (error, response) => {
  if (error) {
    console.error(error);
    return;
  }
  fs.writeFileSync(
    `filteredProductsNoUser${userId}.json`,
    JSON.stringify(response)
  );
  console.log(`Wrote file for user with id ${userId}`);
});
