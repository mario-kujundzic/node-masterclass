const { MongoClient } = require("mongodb");

// Replace the uri string with your connection string.
const uri = "mongodb://127.0.0.1:27017";

const client = new MongoClient(uri);
const database_name = "task_manager";

async function run() {
  const database = client.db(database_name);
  const users = database.collection("users");
  console.log({ users });
  users.insertOne({ name: "Shejv", age: 25 });
}
run().catch(console.dir);
