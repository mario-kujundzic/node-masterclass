// Create examples of two cases where usage of regular functions is preferable one.

// Example 1 - using a function as method inside object

const person = {
  name: "Mario",
  surname: "Kujundzic",
  age: 25,
  getPersonInfo() {
    return `${this.name} ${this.surname} is ${this.age} years old`;
  },
};

console.log(person.getPersonInfo());
person.name = "John";
person.surname = "Smith";
person.age = 39;
console.log(person.getPersonInfo());

// Example 2 - using a function as a constructor, inside class, and also using methods

class Employee {
  constructor(name, surname, age, position) {
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.position = position;
  }
  getEmployeeInfo() {
    return `${this.name} ${this.surname} is ${this.age} years old and working as a ${this.position}`;
  }
}

const emp1 = new Employee("John", "Smith", 39, "JavaScript Junior");
console.log(emp1.getEmployeeInfo());
