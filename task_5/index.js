// Create simple node app. Use process.orgv to get data
// from console input. Symplify the code with yargs.

// sample input
// node .\index.js addProduct --name="Sample name" --description="Description"

const allArgs = process.argv;
const [_node, _programName, operationName, ...params] = allArgs;
const parsedParams = params.map((rawParam) => {
  const paramName = rawParam.split("--")[1].split("=")[0];
  const paramValue = rawParam.split("=")[1];
  return { [paramName]: paramValue };
});
console.log("Adding raw parsed!");
console.log({ operationName, parsedParams });

// simplified

const yargs = require("yargs");

yargs.command({
  command: "addProduct",
  describe: "Add a new product",
  builder: {
    name: {
      describe: "Name of the product",
    },
    description: {
      describe: "Description of the product",
    },
  },
  handler: (argv) => {
    console.log("Adding via yargs!");
    const { _: operationName2, name, description } = argv;
    console.log({ operationName: operationName2, name, description });
  },
});

yargs.parse();
