const product1 = {
  id: 1,
  name: "product1",
  description: "description of product1",
};

const getProduct = () => {
  return { id: 2, name: "product2", description: "description of product2" };
};

// module.exports = { product: product1 };

export { product1 as product, getProduct };

// export default { product: product1, getProduct: getProduct };
// if we use this, can't destructure in statement, must assign to value with 'import x' first, then {product, getProduct} = x
