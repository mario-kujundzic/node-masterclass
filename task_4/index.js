// Create data.txt file and add line “some text” inside it by using fs module
import { writeFileSync } from "fs";

const fileInput = "Some text to be written into the file";
const fileName = "data.txt";

writeFileSync(fileName, fileInput);

// Using OS module grab username of currently logged user and write it in console.

import { userInfo } from "os";

const username = userInfo().username;
console.log(username);

// Write to file “Hello currentUser” using template strings

const fileInput2 = `Greetings, ${username}! Hope you are well.`;
const fileName2 = "greet.txt";

writeFileSync(fileName2, fileInput2);

import { getProduct, product } from "./product.js";

console.log(`Product 1: `, JSON.stringify(product));

console.log(`Product 2: `, JSON.stringify(getProduct()));

import lodash from "lodash";

const { sum } = lodash;

console.log(sum([1, 2, 3]));
