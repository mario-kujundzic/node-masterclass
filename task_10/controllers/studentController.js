const Student = require("../models/student");
const asyncHandler = require("express-async-handler");

// Display list of all Students.
exports.student_list = asyncHandler(async (req, res, next) => {
  res.send("NOT IMPLEMENTED: Student list");
});

// Display detail page for a specific Student.
exports.student_detail = asyncHandler(async (req, res, next) => {
  res.send(`NOT IMPLEMENTED: Student detail: ${req.params.id}`);
});

// Display Student create form on GET.
exports.student_create_get = asyncHandler(async (req, res, next) => {
  res.send("NOT IMPLEMENTED: Student create GET");
});

// Handle Student create on POST.
exports.student_create_post = asyncHandler(async (req, res, next) => {
  res.send("NOT IMPLEMENTED: Student create POST");
});

// Display Student delete form on GET.
exports.student_delete_get = asyncHandler(async (req, res, next) => {
  res.send("NOT IMPLEMENTED: Student delete GET");
});

// Handle Student delete on POST.
exports.student_delete_post = asyncHandler(async (req, res, next) => {
  res.send("NOT IMPLEMENTED: Student delete POST");
});

// Display Student update form on GET.
exports.student_update_get = asyncHandler(async (req, res, next) => {
  res.send("NOT IMPLEMENTED: Student update GET");
});

// Handle Student update on POST.
exports.student_update_post = asyncHandler(async (req, res, next) => {
  res.send("NOT IMPLEMENTED: Student update POST");
});
