const express = require("express");

const studentRouter = require("./routes/students");

const app = express();

app.use("/", studentRouter);

app.listen("3000", () => {
  console.log(`Server started on port ${3000}...`);
});
