const express = require("express");
const router = express.Router();

// Require controller modules.
const student_controller = require("../controllers/studentController");

// GET home page.
router.get("/", function (req, res) {
  res.send("Home page");
});

// GET request for creating Student. NOTE This must come before route for id (i.e. display student).
router.get("/student/create", student_controller.student_create_get);

// POST request for creating Student.
router.post("/student/create", student_controller.student_create_post);

// GET request to delete Student.
router.get("/student/:id/delete", student_controller.student_delete_get);

// POST request to delete Student.
router.post("/student/:id/delete", student_controller.student_delete_post);

// GET request to update Student.
router.get("/student/:id/update", student_controller.student_update_get);

// POST request to update Student.
router.post("/student/:id/update", student_controller.student_update_post);

// GET request for one Student.
router.get("/student/:id", student_controller.student_detail);

// GET request for list of all Students.
router.get("/students", student_controller.student_list);

module.exports = router;
