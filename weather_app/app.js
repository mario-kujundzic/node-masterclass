import request from "postman-request";

const weatherStackApiKey = "26439118b00f9b3f1ea0c58b301797b4";

const mapBoxApiKey =
  "pk.eyJ1Ijoic2hlanYiLCJhIjoiY2toZGtnYWNqMDVvZzJ6azkzbmJ5emYyOSJ9.THcnrL031ZGldTJW7agTQQ";

const weatherStackBaseUrl = "http://api.weatherstack.com/current";

const getWeatherStackCombinedUrl = (lat, lon) =>
  `${weatherStackBaseUrl}?access_key=${weatherStackApiKey}&units=m&query=${lat},${lon}`;

const mapBoxBaseUrl = "http://api.mapbox.com/geocoding/v5/mapbox.places/";

const getMapBoxCombinedUrl = (location) =>
  `${mapBoxBaseUrl}${encodeURIComponent(
    location
  )}.json?access_token=${mapBoxApiKey}&limit=1`;

const forecast = (data, callback) => {
  const { lat, lon, location } = data;
  const url = getWeatherStackCombinedUrl(lat, lon);
  request({ url: url, json: true }, (error, response) => {
    if (error) {
      callback("Error in request", undefined);
      console.log("Unable to execute request");
      return;
    }
    const data = response.body.current;
    const temp = data.temperature;
    const precip = data.precip;
    console.log(
      `${location} - It is currently ${temp} degrees C. There is a ${precip} chance of rain.`
    );
    callback(
      undefined,
      `${location} - It is currently ${temp} degrees C. There is a ${precip} chance of rain.`
    );
  });
};

const geocode = (location, callback) => {
  const url = getMapBoxCombinedUrl(location);
  request({ url: url, json: true }, (error, response) => {
    if (error) {
      callback("Error with call", undefined);
      console.log("Unable to execute request");
      return;
    }
    if (response.body.features.length === 0) {
      callback("Cannot find", undefined);
    }
    const data = {
      lat: response.body.features[0].center[0],
      lon: response.body.features[0].center[1],
      location: response.body.features[0].place_name,
    };
    callback(undefined, data);
  });
};

// Geocoding

geocode("Novi Sad", (error, data) =>
  forecast(data, (error, data) => console.log(data))
);

// Address -> Lat/Long -> Weather
